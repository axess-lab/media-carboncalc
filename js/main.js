const root = document.documentElement;
root.classList.remove('no-js');

const service = document.querySelector("#service");
const hours = document.querySelector("#hours");
const minutes = document.querySelector("#minutes");
const audience = document.querySelector("#audience");
const result = document.querySelector("#result");
const themeColorLight = document.querySelector('meta[name="theme-color"][media="(prefers-color-scheme: light)"');
const themeColorDark = document.querySelector('meta[name="theme-color"][media="(prefers-color-scheme: dark)"');


service.addEventListener("change", () => {
  setTimeout(function () {
    calculateResult();
  }, 100);
});

hours.addEventListener("input", () => {
  setTimeout(function () {
    calculateResult();
  }, 100);
});

minutes.addEventListener("input", () => {
  setTimeout(function () {
    calculateResult();
  }, 100);
});

audience.addEventListener("input", () => {
  setTimeout(function () {
    calculateResult();
  }, 100);
});

function calculateResult() {
  const hoursValue = Number(hours.value);
  const minutesValue = Number(minutes.value);
  const audienceValue = Number(audience.value);
  const serviceValue = Number(
    service.options[service.selectedIndex].dataset.value
  );
  const timeConsumed = hoursValue * 60 + minutesValue;
  let calcResult = timeConsumed * (serviceValue / 60) * audienceValue;
  calcResult = Math.round(calcResult * 1000) / 1000;
  calcResult = calcResult.toString();
  result.innerHTML =
    calcResult + 'kg <span class="co2">Carbon dioxide (CO<sub>2</sub>)</span>';
}

// Restricts input for the given textbox to the given inputFilter.
function setInputFilter(textbox, inputFilter) {
  [
    "input",
    "keydown",
    "keyup",
    "mousedown",
    "mouseup",
    "select",
    "contextmenu",
    "drop"
  ].forEach(function (event) {
    textbox.addEventListener(event, function () {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

// Install input filters.
setInputFilter(hours, function (value) {
  return /^\d*$/.test(value);
});
setInputFilter(minutes, function (value) {
  return /^(?:[0-9]|[0-5][0-9]|)$/.test(value);
});
setInputFilter(audience, function (value) {
  return /^\d*$/.test(value);
});

//Clickable legend (to mimic label behavior
let legends = document.querySelectorAll("legend");
for (let item of legends) {
  item.addEventListener("click", () => {
    const nextInput = item.parentNode.querySelector("input");
    nextInput.focus();
  });
}
//Clickable legend part 2: the fake legend is a fake label
let fakeLegends = document.querySelectorAll(".fake-legend");
for (let item of fakeLegends) {
  item.addEventListener("click", () => {
    const nextInput = item.parentNode.parentNode.parentNode.querySelector(
      "input"
    );
    nextInput.focus();
  });
}

//SVG duration effect
const hourInput = document.querySelector("#hours");
const minuteInput = document.querySelector("#minutes");
const durationFieldset = document.querySelector("#duration-fieldset");
hourInput.addEventListener("focus", () => {
  durationFieldset.classList.add("hour--focus");
});
hourInput.addEventListener("blur", () => {
  durationFieldset.classList.remove("hour--focus");
});
minuteInput.addEventListener("focus", () => {
  durationFieldset.classList.add("minute--focus");
});
minuteInput.addEventListener("blur", () => {
  durationFieldset.classList.remove("minute--focus");
});

//Dark mode
const buttons = `<button type="button" class="button-list no-dark" data-theme="dark" id="themeDark"><span class="sr-only sr-only-focusable"><span>Enable Dark mode</span></span></button>
<button type="button" class="button-list no-dark" data-theme="light" id="themeLight"><span class="sr-only sr-only-focusable"><span>Enable Light mode</span></span></button>`;
const buttonArea = document.querySelector('#lightDark');
buttonArea.innerHTML = buttons;
const btnList = document.querySelectorAll('.button-list');
for (let item of btnList) {
  item.addEventListener('click', () => {
    localStorage.setItem('theme', item.dataset.theme);
    setTheme();
    //themeBtnPressed();
  });
}

/*
function themeBtnPressed(){
  for (let button of btnList) {    
    button.dataset.theme === localStorage.getItem("theme") ?
      button.setAttribute("aria-pressed","true") :
      button.setAttribute("aria-pressed","false");
  }
}
*/

function setTheme(){
  const localTheme = localStorage.getItem('theme');
  if (!localTheme){
    root.dataset.theme = "auto";
    themeColorLight.setAttribute('content','#29435e');
    themeColorDark.setAttribute('content','#000000');
  } else{
    root.dataset.theme = localTheme;
  }
  if(localTheme === "light" ){
    themeColorLight.setAttribute('content','#29435e');
    themeColorDark.setAttribute('content','#29435e');
  } else if(localTheme === "dark" ){
    themeColorLight.setAttribute('content','#000000');
    themeColorDark.setAttribute('content','#000000');
  }
}

document.addEventListener('DOMContentLoaded', () => {
  setTheme();
  //themeBtnPressed();
});
